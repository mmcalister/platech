﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Platech.CognitiveServices.RequestObjects;
using Platech.CognitiveServices.ResponseObjects;

namespace Platech.CognitiveServices
{
    public class MsTextAnalytics
    {
        private static string SubscriptionKey { get; set; }
        private static string URIBase { get; set; }

        // You must use the same Azure region in your REST API method as you used to
        // get your subscription keys. For example, if you got your subscription keys
        // from the West US region, replace "westcentralus" in the URL
        // below with "westus".
        //
        // Free trial subscription keys are generated in the West Central US region.
        // If you use a free trial subscription key, you shouldn't need to change
        // this region.

        public MsTextAnalytics(string subscriptionKey, string uriBase)
        {
            SubscriptionKey = subscriptionKey;
            URIBase = uriBase;
        }

        public async Task<double> GetMinimumSentimentScoreFromTextAsync(IEnumerable<string> textToAnalyze)
        {
            if (textToAnalyze.Any())
            {
                // Call the REST API method.
                var text = await MakeAnalysisRequest(textToAnalyze);
                return text;
            }
            else
            {
                return 0.00;
            }
        }

        private static async Task<double> MakeAnalysisRequest(IEnumerable<string> textToAnalyze)
        {
            try
            {
                HttpClient client = new HttpClient();

                // Request headers.
                client.DefaultRequestHeaders.Add(
                    "Ocp-Apim-Subscription-Key", SubscriptionKey);

                // Request parameters. A third optional parameter is "details".
                // The Analyze Image method returns information about the following
                // visual features:
                // Categories:  categorizes image content according to a
                //              taxonomy defined in documentation.
                // Description: describes the image content with a complete
                //              sentence in supported languages.
                // Color:       determines the accent color, dominant color, 
                //              and whether an image is black & white.
                string requestParameters = "";

                // Assemble the URI for the REST API method.
                string uri = URIBase + "?" + requestParameters;

                HttpResponseMessage response;

                var request = new MsTextAnalyticsSentimentRequestDTO();
                var documents = new List<RequestObjects.Document>();
                foreach (var line in textToAnalyze)
                {
                    documents.Add(new RequestObjects.Document()
                    {
                        id = Guid.NewGuid().ToString(),
                        language = "en",
                        text = line
                    });
                }

                request.documents = documents.ToArray();

                byte[] byteData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(request));

                // Add the byte array as an octet stream to the request body.
                using (ByteArrayContent content = new ByteArrayContent(byteData))
                {
                    // This example uses the "application/octet-stream" content type.
                    // The other content types you can use are "application/json"
                    // and "multipart/form-data".
                    content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/json");

                    // Asynchronously call the REST API method.
                    response = await client.PostAsync(uri, content);
                }

                // Asynchronously get the JSON response.
                string contentString = await response.Content.ReadAsStringAsync();

                // Return the lowest sentiment score.
                return GetLowestScore(contentString);
            }
            catch (Exception e)
            {
                return 0.00;
            }
        }

        private static double GetLowestScore(string contentString)
        {
            if (!String.IsNullOrEmpty(contentString))
            {
                var msTextAnalyticsSentimentResults = JsonConvert.DeserializeObject<MsTextAnalyticsSentimentResponseDTO>(contentString);

                if (msTextAnalyticsSentimentResults != null && msTextAnalyticsSentimentResults.documents.Any())
                    return msTextAnalyticsSentimentResults.documents.Min(item => item.score);
                else
                    return 0.0;
            }
            return 0.00;
        }
    }
}

namespace Platech.CognitiveServices.RequestObjects
{
    public class MsTextAnalyticsSentimentRequestDTO
    {
        public Document[] documents { get; set; }
    }

    public class Document
    {
        public string language { get; set; }
        public string id { get; set; }
        public string text { get; set; }
    }
}

namespace Platech.CognitiveServices.ResponseObjects
{
    public class MsTextAnalyticsSentimentResponseDTO
    {
        public Document[] documents { get; set; }
        public object errors { get; set; }
    }

    public class Document
    {
        public float score { get; set; }
        public string id { get; set; }
    }
}
