﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Platech.CognitiveServices
{
    public class MsOcr
    {
        private static string SubscriptionKey { get; set; }
        private static string URIBase { get; set; }

        public MsOcr()
        {

        }

        public MsOcr(string subscriptionKey, string uriBase)
        {
            SubscriptionKey = subscriptionKey;// = "63aa00a9e28f47fc825ea2f38181bd8a";
            URIBase = uriBase;
        }
        // sample Computer Vision keys valid till 11/13/2018
        //Key 1: 63aa00a9e28f47fc825ea2f38181bd8a
        //Key 2: 654a71243bee4ede98680c1f4f17f32d

        // Replace <Subscription Key> with your valid subscription key.

        // You must use the same Azure region in your REST API method as you used to
        // get your subscription keys. For example, if you got your subscription keys
        // from the West US region, replace "westcentralus" in the URL
        // below with "westus".
        //
        // Free trial subscription keys are generated in the West Central US region.
        // If you use a free trial subscription key, you shouldn't need to change
        // this region.

        public async Task<IEnumerable<string>> GetTextFromImageAsync(byte[] imageData)
        {

            var text = await MakeAnalysisRequest(imageData);
            return text;

        }

        private static async Task<IEnumerable<string>> MakeAnalysisRequest(byte[] imageData)
        {
            try
            {
                HttpClient client = new HttpClient();

                // Request headers.
                client.DefaultRequestHeaders.Add(
                    "Ocp-Apim-Subscription-Key", SubscriptionKey);

                // Request parameters. A third optional parameter is "details".
                // The Analyze Image method returns information about the following
                // visual features:
                // Categories:  categorizes image content according to a
                //              taxonomy defined in documentation.
                // Description: describes the image content with a complete
                //              sentence in supported languages.
                // Color:       determines the accent color, dominant color, 
                //              and whether an image is black & white.
                string requestParameters = "";

                // Assemble the URI for the REST API method.
                string uri = URIBase + "?" + requestParameters;

                HttpResponseMessage response;

                // Read the contents of the specified local image
                // into a byte array.
                byte[] byteData = imageData;//GetImageAsByteArray(imageFilePath);

                // Add the byte array as an octet stream to the request body.
                using (ByteArrayContent content = new ByteArrayContent(byteData))
                {
                    // This example uses the "application/octet-stream" content type.
                    // The other content types you can use are "application/json"
                    // and "multipart/form-data".
                    content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");

                    // Asynchronously call the REST API method.
                    response = await client.PostAsync(uri, content);
                }

                // Asynchronously get the JSON response.
                string contentString = await response.Content.ReadAsStringAsync();

                // Return the list of words recognized.
                return GetArrayOfTexts(contentString);
            }
            catch (Exception e)
            {
                return new string[] { e.Message };
            }
        }

        private static IEnumerable<string> GetArrayOfTexts(string contentString)
        {
            var ocrResults = JsonConvert.DeserializeObject<MsOcrDTO>(contentString);

            foreach(var region in ocrResults.regions)
            {
                foreach(var line in region.lines)
                {
                    foreach(var word in line.words)
                    {
                        yield return word.text;
                    }
                }
            }
        }

        /// <summary>
        /// Returns the contents of the specified file as a byte array.
        /// </summary>
        /// <param name="imageFilePath">The image file to read.</param>
        /// <returns>The byte array of the image data.</returns>
        static byte[] GetImageAsByteArray(string imageFilePath)
        {
            // Open a read-only file stream for the specified file.
            using (FileStream fileStream =
                new FileStream(imageFilePath, FileMode.Open, FileAccess.Read))
            {
                // Read the file's contents into a byte array.
                BinaryReader binaryReader = new BinaryReader(fileStream);
                return binaryReader.ReadBytes((int)fileStream.Length);
            }
        }
    }


    public class MsOcrDTO
    {
        public string language { get; set; }
        public float textAngle { get; set; }
        public string orientation { get; set; }
        public Region[] regions { get; set; }
    }

    public class Region
    {
        public string boundingBox { get; set; }
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public string boundingBox { get; set; }
        public Word[] words { get; set; }
    }

    public class Word
    {
        public string boundingBox { get; set; }
        public string text { get; set; }
    }

}
