﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Azure.CognitiveServices.ContentModerator;
using Microsoft.CognitiveServices.ContentModerator;
using Microsoft.CognitiveServices.ContentModerator.Models;
using System.Threading.Tasks;
using System.Linq;

namespace Platech.CognitiveServices
{
    public class MsContentModerator
    {
        private static string SubscriptionKey { get; set; }
        private static string URIBase { get; set; }

        // You must use the same Azure region in your REST API method as you used to
        // get your subscription keys. For example, if you got your subscription keys
        // from the West US region, replace "westcentralus" in the URL
        // below with "westus".
        //
        // Free trial subscription keys are generated in the West Central US region.
        // If you use a free trial subscription key, you shouldn't need to change
        // this region.

        public MsContentModerator(string subscriptionKey, string uriBase)
        {
            SubscriptionKey = subscriptionKey;// = "63aa00a9e28f47fc825ea2f38181bd8a";
            URIBase = uriBase;
        }

        public ContentModeratorClient CreateClient()
        {
            // Create and initialize an instance of the Content Moderator API wrapper.
            ContentModeratorClient client = new ContentModeratorClient(new ApiKeyServiceClientCredentials(SubscriptionKey));

            client.Endpoint = URIBase;
            return client;
        }

        public async Task<bool> GetReviewRecommendation(IEnumerable<string> textToAnalyze)
        {
            if (textToAnalyze.Any())
            {
                // Call the REST API method.
                var review = await MakeAnalysisRequest(textToAnalyze);
                return review;
            }
            else
            {
                return true;
            }
        }

        private async Task<bool> MakeAnalysisRequest(IEnumerable<string> textToAnalyze)
        {
            try
            {
                // Load the input text.
                var text = String.Join("", textToAnalyze).Replace(System.Environment.NewLine, " ");
                byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(text);
                MemoryStream stream = new MemoryStream(byteArray);

                // Create a Content Moderator client and evaluate the text.
                using (var client = CreateClient())
                {
                    // Screen the input text: check for profanity,
                    // autocorrect text, check for personally identifying
                    // information (PII), and classify the text into three categories
                    var screenResult = client.TextModeration.ScreenText("text/plain", stream, "eng", true, true, null, true);

                    return screenResult.Classification.ReviewRecommended ?? true;
                }
            }
            catch (Exception e)
            {
                return true;
            }
        }
    }
}
