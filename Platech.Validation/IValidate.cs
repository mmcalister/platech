﻿using Platech.DataLayer;

namespace Platech.Validation
{
    internal interface IValidate
    {
        bool Validate(string textToValidate, State state);
    }
}