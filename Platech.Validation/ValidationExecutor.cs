﻿using Platech.DataLayer;
using System;
using System.Linq;

namespace Platech.Validation
{
    public class ValidationExecutor
    {
        public bool ExecuteValidation(string textToValidate, State state)
        {
            var type = typeof(IValidate);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface);

            foreach(var t in types)
            {
                var v = Activator.CreateInstance(t) as IValidate;
                if (!v.Validate(textToValidate, state))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
