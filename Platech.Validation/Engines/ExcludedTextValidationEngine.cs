﻿using Platech.DataLayer;
using System.Threading.Tasks;

namespace Platech.Validation.Engines
{
    class ExcludedTextValidationEngine : IValidate
    {
        public bool Validate(string textToValidate, State state)
        {
            bool isValid = true;
            var stateRegexExpressions = MockDatabase.AllStatesCensoredWords[state];
            Parallel.ForEach(stateRegexExpressions, (expression, loopState) =>
            {
                if (textToValidate.Contains(expression))
                {
                    loopState.Stop();
                    isValid = false;
                    return;
                }
            });

            return isValid;
        }
    }
}
