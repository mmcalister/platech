﻿using Platech.DataLayer;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Platech.Validation.Engines
{
    class RegexValidationEngine : IValidate
    {
        public bool Validate(string textToValidate, State state)
        {
            bool isValid = true;
            var stateRegexExpressions = MockDatabase.AllStatesRegexs[state];
            Parallel.ForEach(stateRegexExpressions, (expression, loopState) =>
            {
                if (Regex.IsMatch(textToValidate, expression))
                {
                    loopState.Stop();
                    isValid = false;
                    return;
                }
            });

            return isValid;
        }
    }
}
