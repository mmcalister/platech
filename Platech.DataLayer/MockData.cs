﻿using System.Collections.Generic;

namespace Platech.DataLayer
{
    public class MockData
    {
        public static Dictionary<State, string> GetDefaultStateList()
        {
            var items = new Dictionary<State, string>
            {
                { State.LA, "Louisiana" },
                { State.MS, "Mississippi" },
                { State.VA, "Virginia" },
                { State.WI, "Wisconsin" }
            };

			return items;
		}

		public static IDictionary<string, string> GetPlateTypes()
		{
			var items = new Dictionary<string, string>{
				{ State.LA.ToString(), "Scenic" },
				{ State.MS.ToString(), "Original" }
			};

			return items;
		}

        public static Dictionary<State, List<string>> GetAllDefaultStateExcludedWords()
        {
            var allStateCensoredWords = new Dictionary<State, List<string>>();

            allStateCensoredWords.Add(State.LA, GetStateExcludedWords(State.LA));
            allStateCensoredWords.Add(State.MS, GetStateExcludedWords(State.MS));
            allStateCensoredWords.Add(State.VA, GetStateExcludedWords(State.VA));
            allStateCensoredWords.Add(State.WI, GetStateExcludedWords(State.WI));

            return allStateCensoredWords;
        }
        private static List<string> GetStateExcludedWords(State state)
        {
            var items = new List<string>();

            switch (state)
            {
                case State.LA:
                    items.Add("LALA");
                    items.Add("LOUISIANA");
                    break;
                    
                case State.MS:
                    items.Add("MISSY");
                    items.Add("MISSISSIPPI");
                    break;

                case State.VA:
                    items.Add("VIRGINIA");
                    break;

                case State.WI:
                    items.Add("WISS");
                    break;

                default:
                    break;
            }

            items.Add("POLICE");
            items.Add("PRESIDENT");

            return items;
        }

        public static Dictionary<State, List<string>> GetAllDefaultStateExcludedRegexs()
        {
            var result = new Dictionary<State, List<string>>();

            result.Add(State.WI, GetDefaultStateExcludedRegexs(State.WI));
            result.Add(State.LA, GetDefaultStateExcludedRegexs(State.LA));
            result.Add(State.MS, GetDefaultStateExcludedRegexs(State.MS));
            result.Add(State.VA, GetDefaultStateExcludedRegexs(State.VA));

            return result;
        }

        public static List<string> GetDefaultStateExcludedRegexs(State state)
        {
            var items = new List<string>();

            switch (state)
            {
                case State.LA:
                    items.Add("^C.*$");     //starts with C is not allowed in Louisiana. this is reserved for Commercial plates
                    break;

                case State.VA:
                    items.Add("^C.*$");     //starts with C is not allowed in VA. this is reserved for Commercial plates
                    break;

                default:
                    break;
            }

            items.Add("^911.*$");     //starts with 911 is not allowed anywhere?

            return items;
        }

		public IDictionary<string, string> GetCharacterReplacements()
		{
			var items = new Dictionary<string, string>();
			items.Add( "O", "0" );
			items.Add( "0", "O" );
			items.Add( "1", "I" );
			items.Add( "I", "1" );
			return items;
		}
	}
}
