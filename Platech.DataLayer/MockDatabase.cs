﻿using System;
using System.Collections.Generic;

namespace Platech.DataLayer
{
    public static class MockDatabase
    {
        public static Dictionary<State, string> States { get; set; }

        public static Dictionary<State, List<string>> AllStatesRegexs { get; set; }

        public static Dictionary<State, List<string>> AllStatesCensoredWords { get; set; }

        static MockDatabase()
        {
            States = MockData.GetDefaultStateList();
            AllStatesRegexs = MockData.GetAllDefaultStateExcludedRegexs();
            AllStatesCensoredWords = MockData.GetAllDefaultStateExcludedWords();
        }
    }
}
