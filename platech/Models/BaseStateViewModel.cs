﻿using Platech.DataLayer;

namespace platech.Models
{
    public class BaseStateViewModel
    {
        public State State { get; set; }
    }
}
