﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Internal;

namespace platech.Models
{
	public class PlateResultViewModel : PlatePreviewViewModel
	{
		public IEnumerable<string> OcrText { get; set; }

		[Display( Name = "Sentiment Score" )]
		public double SentimentScore { get; set; }

		[Display( Name = "Moderation Profantity Score" )]
		public double ModerationProfantityScore { get; set; }

		[Display( Name = "Flag for human review" )]
		public bool ReviewRecommended { get; set; }

        public string PlateImage { get; set; }

        [Display( Name = "OCR Text" )]
		public string OcrTextAsString
		{
			get
			{
				if ( OcrText != null && OcrText.Any() )
					return String.Join( ",", OcrText );
				return PlateNumber;
			}
		}

        [Display(Name = "Restricted by State")]
        public bool IsStateRestricted { get; set; }
    }
}
