﻿using Platech.DataLayer;
using System.Collections.Generic;

namespace platech.Models
{
    public class StatePlateManagerViewModel : BaseStateViewModel
    {
        public string StateName { get; set; }

        public IEnumerable<string> PlateRegexs { get; set; }

        public IEnumerable<string> CensoredWords { get; set; }
    }
}
