﻿namespace platech.Models
{
    public class AddCensoredWordViewModel : BaseStateViewModel
    {
        public string CensoredWord { get; set; }
    }
}
