﻿using Platech.DataLayer;

namespace platech.Models
{
    public class CreateRegexViewModel : BaseStateViewModel
    {
        public string NewRegex { get; set; }
    }
}
