﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace platech.Models
{
	public class PlatePreviewViewModel
	{
		[Display( Name = "Plate State" )]
		[Required, RegularExpression( "[a-zA-Z0-9]*", ErrorMessage = "Please enter only letters and numbers." )]
		[MaxLength( 2 )]
		public string StateCode { get; set; }

		[Display( Name = "Plate Type" )]
		[Required, RegularExpression( "[a-zA-Z0-9]*", ErrorMessage = "Please enter only letters and numbers." )]
		[MaxLength( 10 )]
		public string PlateType { get; set; }

		[Display( Name = "Plate Text" )]
		[Required, RegularExpression( "[a-zA-Z0-9\\s]{1,10}", ErrorMessage = "Please enter only letters and numbers." )]
		[MaxLength( 10 )]
		public string PlateNumber { get; set; }

		public SelectList StateSelectList { get; set; }

		public SelectList PlateTypeList { get; set; }
	}
}
