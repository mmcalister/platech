﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace platech.Models
{
    public class CognitiveServicesInfoModel
    {
        public string Title { get; set; }
        public ServiceKey ServiceKey{ get; set; }
    }

    public class ServiceKey
    {
        public string PrimaryAPIKey { get; set; }
        public string SecondaryAPIKey { get; set; }
        public string URIBase { get; set; }
    }
}
