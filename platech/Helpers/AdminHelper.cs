﻿using Platech.DataLayer;
using System.Linq;

namespace platech.Helpers
{
    public class AdminHelper
    {
        public static void AddRegexToState(State state, string regex)
        {
            MockDatabase.AllStatesRegexs.First(x => x.Key == state).Value.Add(regex);
        }

        public static void AddCensoredWordToState(State state, string word)
        {
            MockDatabase.AllStatesCensoredWords.First(x => x.Key == state).Value.Add(word);
        }
    }
}
