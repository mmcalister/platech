﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using platech.Models;
using Platech.CognitiveServices;
using System.Text;

namespace platech.Controllers
{
    public class TestController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        //public IActionResult About()
        //{
        //    var imagePath = @"C:\Users\matthew.mcalister\Desktop\vulgur_v.jpeg";


        //    var ocrText = new MsOcr().GetTextFromImageAsync(imagePath);
        //    ocrText.Wait();
        //    var ocrRecognizedWords = ocrText.Result;

        //    StringBuilder sb = new StringBuilder();
        //    foreach(var word in ocrRecognizedWords)
        //    {
        //        sb.AppendLine(word);
        //    }


        //    var testing = new MsTextAnalytics().GetMinimumSentimentScoreFromTextAsync(ocrRecognizedWords);
        //    testing.Wait();
        //    var gggggg = testing.Result;


        //    ViewData["Message"] = sb.ToString();

        //    return View("Index");
        //}

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
