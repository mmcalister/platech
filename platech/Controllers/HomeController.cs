﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using platech.Models;
using Platech.CognitiveServices;
using Platech.DataLayer;
using Platech.Validation;

namespace platech.Controllers
{
	public class HomeController : Controller
	{
        private IConfiguration _configuration;

        public HomeController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

		//private static string assemblyFolder = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location );
		//private static string defaultLouisianaPlatePath = Path.Combine( assemblyFolder, "Images", "LA", "la_plate_blank.jpeg" );
		//private static string defaultVirginiaPlatePath = Path.Combine( assemblyFolder, "Images", "VA", "va_plate_v.jpeg" );
		//private static string defaultWIPlatePath = Path.Combine( assemblyFolder, "Images", "WI", "wi_plate_blank.jpeg" );


		//      public IActionResult Test()
		//{

		//	var imagePath = defaultVirginiaPlatePath; //defaultLouisianaPlatePath;

		//	var ocrText = new MsOcr().GetTextFromImageAsync( imagePath );
		//	ocrText.Wait();
		//	var ocrRecognizedWords = ocrText.Result;

		//	StringBuilder sb = new StringBuilder();
		//	foreach ( var word in ocrRecognizedWords )
		//	{
		//		sb.AppendLine( word );
		//	}

		//	var sentiment = new MsTextAnalytics().GetMinimumSentimentScoreFromTextAsync( ocrRecognizedWords );
		//	sentiment.Wait();
		//	var sentimentValue = sentiment.Result;

		//	var contentModerator = new MsContentModerator().GetReviewRecommendation( ocrRecognizedWords );
		//	contentModerator.Wait();
		//	var reviewRecommendation = contentModerator.Result;

		//	foreach ( var word in ocrRecognizedWords )
		//	{
		//		new ValidationExecutor().ExecuteValidation( word, "LA" );
		//	}

		//	ViewData["Message"] = sb.ToString();

		//	return View();
		//}

		[HttpGet]
		public IActionResult Index()
		{
			var model = new PlateResultViewModel {
				StateSelectList = new SelectList( MockDatabase.States, "Key", "Value" ),
				PlateTypeList = new SelectList( MockData.GetPlateTypes(), "Key", "Value" )
			};

			return View( model );
		}

		[HttpPost]
		public async Task<IActionResult> Index( PlateResultViewModel model )
		{
			//todo: model validation

			model.PlateNumber = model.PlateNumber.ToUpper();
			model.StateSelectList = new SelectList( MockDatabase.States, "Key", "Value" );
			model.PlateTypeList = new SelectList( MockData.GetPlateTypes(), "Key", "Value" );
			model.IsStateRestricted = false;

            var contentModeratorAPIKey = _configuration.GetSection("CognitiveServices").GetSection("ContentModerator").GetValue<string>("PrimaryAPIKey");
            var contentModeratorURI = _configuration.GetSection("CognitiveServices").GetSection("ContentModerator").GetValue<string>("URIBase");
            var computerVisionAPIKey = _configuration.GetSection("CognitiveServices").GetSection("ComputerVision").GetValue<string>("PrimaryAPIKey");
            var computerVisionURI = _configuration.GetSection("CognitiveServices").GetSection("ComputerVision").GetValue<string>("URIBase");
            var textAnalyticsAPIKey = _configuration.GetSection("CognitiveServices").GetSection("TextAnalytics").GetValue<string>("PrimaryAPIKey");
            var textAnalyticsURI = _configuration.GetSection("CognitiveServices").GetSection("TextAnalytics").GetValue<string>("URIBase");

            var msOcr = new MsOcr(computerVisionAPIKey, computerVisionURI);
			var msContentModerator = new MsContentModerator(contentModeratorAPIKey, contentModeratorURI);
			var msTextAnalytics = new MsTextAnalytics(textAnalyticsAPIKey, textAnalyticsURI);
			var plateImageBase64 = model.PlateImage?.Replace( "data:image/png;base64,", String.Empty );
			var imageAsBytes = Convert.FromBase64String( plateImageBase64 );


			//model.ReviewRecommended = await msContentModerator.GetReviewRecommendation(model.OcrText);

			//model.SentimentScore = await msTextAnalytics.GetMinimumSentimentScoreFromTextAsync(model.OcrText);


			if ( model.StateCode == "VA" || model.StateCode == "WI" )
			{
				model.OcrText = await msOcr.GetTextFromImageAsync( imageAsBytes );

				foreach ( var word in model.OcrText )
				{
                    var state = MockDatabase.States.Keys.First(x => x.ToString() == model.StateCode);
					if ( !new ValidationExecutor().ExecuteValidation( word, state) )
					{
						model.IsStateRestricted = true;
						break;
					}
				}

				model.ReviewRecommended = await msContentModerator.GetReviewRecommendation( model.OcrText );
				model.SentimentScore = await msTextAnalytics.GetMinimumSentimentScoreFromTextAsync( model.OcrText );
			}
			else
			{
				var text = new[] { model.PlateNumber };

				foreach ( var word in text )
				{
                    var state = MockDatabase.States.Keys.First(x => x.ToString() == model.StateCode);
                    if ( !new ValidationExecutor().ExecuteValidation( word, state ) )
					{
						model.IsStateRestricted = true;
						break;
					}
				}

				//model.OcrText = msOcr.GetTextFromImageAsync(defaultVirginiaPlatePath).Result;
				model.ReviewRecommended = await msContentModerator.GetReviewRecommendation( text );
				model.SentimentScore = await msTextAnalytics.GetMinimumSentimentScoreFromTextAsync( text );
			}

			// PG fake response
			if ( model.PlateNumber == "JAYHAWK" )
			{
				model.ReviewRecommended = true;
				model.SentimentScore = .001;
			}

			return View( model );
		}
	}
}