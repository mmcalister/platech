﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using platech.Helpers;
using platech.Models;
using Platech.DataLayer;

namespace platech.Controllers
{
    public class AdminController : Controller
    {
        [HttpGet]
        public IActionResult Index(State state)
        {
            var viewModel = GetViewModelForState(state);

            AddStateList(state);
            return View(viewModel);
        }

        [HttpGet]
        public IActionResult CreateRegex(State state)
        {
            var viewModel = new CreateRegexViewModel
            {
                State = state
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult CreateRegex(CreateRegexViewModel model)
        {
            AdminHelper.AddRegexToState(model.State, model.NewRegex);
            return RedirectToAction("Index", new { state = model.State });
        }

        [HttpGet]
        public IActionResult AddCensoredWord(State state)
        {
            var viewModel = new AddCensoredWordViewModel
            {
                State = state
            };

            return View(viewModel);
        }
        [HttpPost]
        public IActionResult AddCensoredWord(AddCensoredWordViewModel model)
        {
            AdminHelper.AddCensoredWordToState(model.State, model.CensoredWord);
            return RedirectToAction("Index", new { state = model.State });
        }

        private StatePlateManagerViewModel GetViewModelForState(State state)
        {
            var viewModel = new StatePlateManagerViewModel
            {
                State = MockDatabase.States.First(x => x.Key == state).Key,
                StateName = MockDatabase.States.First(x => x.Key == state).Value,
                CensoredWords = MockDatabase.AllStatesCensoredWords.First(x => x.Key == state).Value,
                PlateRegexs = MockDatabase.AllStatesRegexs.First(x => x.Key == state).Value
            };

            return viewModel;
        }

        private void AddStateList(State selectedState)
        {
            // State enum int as the value
            ViewBag.StateList = MockDatabase.States.Select(x => 
                new SelectListItem(x.Value, ((int)x.Key).ToString(), x.Key == selectedState));
        }
    }
}