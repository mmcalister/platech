﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using platech.Models;
using Platech.CognitiveServices;
using Platech.DataLayer;
using Platech.Validation;

namespace platech.Controllers
{
    public class PlateController : Controller
    {
        public IActionResult Index()
        {
            //string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            //string defaultLouisianaPlatePath = Path.Combine(assemblyFolder, "Images", "LA", "la_plate_blank.jpeg");
            //string defaultVirginiaPlatePath = Path.Combine(assemblyFolder, "Images", "VA", "va_plate_v.jpeg");


            //var imagePath = defaultVirginiaPlatePath; //defaultLouisianaPlatePath;

            ////var ocrText = new MsOcr().GetTextFromImageAsync(imagePath);
            ////ocrText.Wait();
            ////var ocrRecognizedWords = ocrText.Result;

            //StringBuilder sb = new StringBuilder();
            //foreach (var word in ocrRecognizedWords)
            //{
            //    sb.AppendLine(word);
            //}

            //var sentiment = new MsTextAnalytics().GetMinimumSentimentScoreFromTextAsync(ocrRecognizedWords);
            //sentiment.Wait();
            //var sentimentValue = sentiment.Result;

            //var contentModerator = new MsContentModerator().GetReviewRecommendation(ocrRecognizedWords);
            //contentModerator.Wait();
            //var reviewRecommendation = contentModerator.Result;

            //foreach(var word in ocrRecognizedWords)
            //{
            //    new ValidationExecutor().ExecuteValidation(word, State.LA);
            //}

            //ViewData["Message"] = sb.ToString();

            return View();
        }

        public IActionResult Viewer()
        {
            var model = new PlatePreviewViewModel();
            model.StateSelectList = new SelectList(MockDatabase.States.ToDictionary(x => x.Key.ToString(), x => x.Value), "Key", "Value");

            return View(model);
        }
    }
}